﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Web.API;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Broswer.Test.IntegrationTest.IntegrationTest.API.BookTest
{
    /// <summary>
    /// Prueba de prueba para el metodo de obtener listado de libros
    /// </summary>
    [TestFixture]
    public class GetAllIntegrationTest : IntegrationApiTestBase
    {
        #region Constantes

        private const string API_BOOK = "api/Book?";

        #endregion

        #region Propiedades

        private static HttpClient _client;
        private static WebApplicationFactory<Startup> _factory;
        private IEnumerable<Book> _books;
        private Editorial _editorial;
        private List<Author> _authors;

        #endregion

        #region Constructor

        public GetAllIntegrationTest()
        {
            _factory = new WebApplicationFactory<Startup>();
            _client = new HttpClient();
            _client.Timeout = System.TimeSpan.FromMinutes(5);
        }

        #endregion

        #region Inicio

        [SetUp]
        public async Task Setup()
        {            
            string token = GenerateJwtToken();
            _books = await CreateListBook(1, 400);
            _client = _factory.CreateClient();
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        #endregion

        #region Limpiar

        [TearDown]
        public void Cleanup()
        {
            if (_books != null)
            {
                using (var scope = _factory.Services.CreateScope())
                {
                    IBookRepository book = scope.ServiceProvider.GetRequiredService<IBookRepository>();
                    foreach (Book item in _books)
                    {
                        book.DeleteBookAsync(item.ISBN);
                    }
                }
            }
        }

        #endregion

        #region Pruebas

        /// <summary>
        /// Metodo para obtener status code Ok con paginacion
        /// </summary>
        [Test]
        public async Task Get_Books_Should_Response_StatusCode_OK()
        {
            int pageNumber = 5;
            int pageSize = 3;

            HttpResponseMessage response = await _client.GetAsync($"{API_BOOK}PageNumber={pageNumber}&PageSize={pageSize}");

            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        #endregion

        #region Metodos privados

        /// <summary>
        /// Metodo para crear una lista de libros de prueba en la base de datos.
        /// </summary>
        /// <param name="start">Indicador</param>
        /// <param name="count">Cantidad</param>
        /// <returns>Retorna lista de libros</returns>
        private async Task<IEnumerable<Book>> CreateListBook(int start, int count)
        {
            using (IServiceScope scope = _factory.Services.CreateScope())
            {
                IBookRepository bookRepository = scope.ServiceProvider.GetRequiredService<IBookRepository>();
                List<Book> bookList = new List<Book>();
                _authors = CreateListAuthor();
                _editorial = await CreateEditorial();

                foreach (Book book in Enumerable.Range(start, count).Select<int, Book>(x => new Book()
                {
                    ISBN = x,
                    Title = $"Titulo {x}",
                    Synapse = $"Sinapsis {x}",
                    NumberPages = $"{x}",
                    EditorialId = _editorial.Id,
                    AuthorsHasBooks = new List<AuthorHasBook>()
                }))
                {
                    await bookRepository.CreateBookAsync(book, _authors);
                    bookList.Add(book);
                }               

                return bookList;
            }
        }

        /// <summary>
        /// Metodo para crear una lista de autores de prueba en la base de datos.
        /// </summary>
        /// <returns>Retorna lista de autorers</returns>
        private List<Author> CreateListAuthor()
        {                
            List<Author> authorList = new List<Author>();
                
            Author author = new Author
            {
                Name = "Author Prueba",
                Surname = "Auhtor Prueba"
            };

            authorList.Add(author);

            return authorList;     
        }

        /// <summary>
        /// Metodo para crear una editarial de prueba en la base de datos.
        /// </summary>
        /// <returns>Retorna editorial</returns>
        private async Task<Editorial> CreateEditorial()
        {
            using (IServiceScope scope = _factory.Services.CreateScope())
            {
                IEditorialRepository editorialRepository = scope.ServiceProvider.GetRequiredService<IEditorialRepository>();
                Editorial editorialRepo = await editorialRepository.CreateEditorialAsync(new Editorial 
                {
                    Name = "Editorial prueba",
                    Campus = "Sede prueba"
                });

                return editorialRepo;
            }
        }

        /// <summary>
        /// Metodo para generar Token de autorizacion con JWT
        /// </summary>
        /// <returns>Retorna Token de Autorizacion</returns>
        private string GenerateJwtToken()
        {
            SymmetricSecurityKey _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(GetConfiguration()["Authentication:SecretKey"]));
            SigningCredentials signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            JwtHeader header = new JwtHeader(signingCredentials);

            Claim[] claims = new[]
            {
                new Claim( ClaimTypes.Name, "Jonathan Burgos"),
                new Claim( ClaimTypes.Email, "jonathanburgosgonzalez@gmail.com"),
                new Claim( ClaimTypes.Role, "Administrador"),
            };

            JwtPayload payload = new JwtPayload(GetConfiguration()["Authentication:Issuer"],
                GetConfiguration()["Authentication:Audience"],
                claims,
                DateTime.Now,
                DateTime.UtcNow.AddMinutes(5000));

            JwtSecurityToken token = new JwtSecurityToken(header, payload);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        #endregion
    }
}
