﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Web.API;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Broswer.Test.IntegrationTest.IntegrationTest.API.BookTest
{
    /// <summary>
    /// Prueba de prueba para el metodo de obtener detalle del libro
    /// </summary>
    [TestFixture]
    public class GetDetailIntegrationTest
    {
        #region Constantes

        private const string API_BOOK = "api/Book?";

        #endregion

        #region Propiedades

        private static HttpClient _client;
        private static WebApplicationFactory<Startup> _factory;

        #endregion

        #region Constructor

        public GetDetailIntegrationTest()
        {
            _factory = new WebApplicationFactory<Startup>();
            _client = new HttpClient();
            _client.Timeout = System.TimeSpan.FromMinutes(5);
        }

        #endregion

        #region Inicio

        [SetUp]
        public async Task Setup()
        {
            _client = _factory.CreateClient();
        }

        #endregion

        #region Pruebas

        /// <summary>
        /// Metodo para obtener status code Unauthorized
        /// </summary>
        [Test]
        public async Task Get_Books_Should_Response_StatusCode_Unauthorized()
        {

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", null);
            HttpResponseMessage response = await _client.GetAsync($"{API_BOOK}");

            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        #endregion
    }
}
