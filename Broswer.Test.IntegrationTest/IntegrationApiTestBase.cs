﻿using Microsoft.Extensions.Configuration;
using System;

namespace Broswer.Test.IntegrationTest
{
    public class IntegrationApiTestBase
    {
        protected IntegrationApiTestBase()
        {
            string connectionString = GetConfiguration().GetSection("ConnectionStrings:DefaultConnetion").Value;

            string isTest = GetConfiguration().GetSection("IsTest").Value;

            Environment.SetEnvironmentVariable("ConnectionStrings", connectionString);
            Environment.SetEnvironmentVariable("IsTest", isTest);
        }

        public static IConfiguration GetConfiguration()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
    }
}
