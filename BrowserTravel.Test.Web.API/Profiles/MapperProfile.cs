﻿using AutoMapper;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;

namespace BrowserTravel.Test.API.Profiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Book, BookDto>();
            CreateMap<BookDto, Book>();
            CreateMap<Author, AuthorDto>();
            CreateMap<AuthorDto, Author>();
            CreateMap<Editorial, EditorialDto>();
            CreateMap<EditorialDto, Editorial>();
        }
    }
}
