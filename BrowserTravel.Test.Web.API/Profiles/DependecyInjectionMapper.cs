﻿using AutoMapper;

namespace BrowserTravel.Test.API.Profiles
{
    public static class DependecyInjectionMapper
    {
        public static MapperConfiguration GetMapperConfig()
        {
            return new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapperProfile());
            });
        }
    }
}
