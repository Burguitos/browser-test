﻿using BrowserTravel.Test.API.Middlewares.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BrowserTravel.Test.API.Middlewares
{
    /// <summary>
    /// Clase middleware de manejo de expeciones
    /// </summary>
    public class ExceptionMiddleware
    {
        private readonly ILogger<ExceptionMiddleware> _logger;
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        /// <summary>
        /// Metodo de invocaion de excepciones
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Internal Error: {ex.Message}-{httpContext.GetEndpoint()}");

                await HandleExceptionAsync(httpContext);
            }
        }

        /// <summary>
        /// Metodo handler para capturar la excepcion
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Retorna el error y el codigo de estado 500</returns>
        private async Task HandleExceptionAsync(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = "Internal Server Error"
            }.ToString());
        }
    }
}
