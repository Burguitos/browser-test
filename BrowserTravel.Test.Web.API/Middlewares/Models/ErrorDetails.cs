﻿using System.Text.Json;

namespace BrowserTravel.Test.API.Middlewares.Models
{
    /// <summary>
    /// Modelo de detalle de error para las execpciones
    /// </summary>
    public class ErrorDetails
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }

        /// <summary>
        /// Metodo sobrescrito para serializar en Json
        /// </summary>
        /// <returns>Retorna Json string</returns>
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
