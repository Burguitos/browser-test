﻿using BrowserTravel.Test.API.Middlewares.Models;
using BrowserTravel.Test.Core.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.API.Controllers
{
    /// <summary>
    /// Controlador de token
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Metodo para autorizar 
        /// </summary>
        /// <param name="userLogin">Entidad de usuario de incio de sesion</param>
        /// <returns>Retorna Token de autenticacion</returns>
        /// <response code="200">Obtener token satisfactoriamente</response>
        /// <response code="404">Usuario no valido</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Authentication(UserLogin userLogin)
        {
            if (await IsValidUser(userLogin))
            {
                string token = GenerateToken();
                return Ok(new { token });
            }

            return NotFound(new ErrorDetails
            {
                StatusCode = (int)HttpStatusCode.NotFound,
                Message = "Usuario no valido"
            });
        }

        private async Task<bool> IsValidUser(UserLogin userLogin)
        {
            return true;
        }

        private string GenerateToken()
        {
            SymmetricSecurityKey _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:SecretKey"]));
            SigningCredentials signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            JwtHeader header = new JwtHeader(signingCredentials);

            Claim[] claims = new[]
            {
                new Claim( ClaimTypes.Name, "Jonathan Burgos"),
                new Claim( ClaimTypes.Email, "jonathanburgosgonzalez@gmail.com"),
                new Claim( ClaimTypes.Role, "Administrador"),
            };

            JwtPayload payload = new JwtPayload(_configuration["Authentication:Issuer"], 
                _configuration["Authentication:Audience"], 
                claims, 
                DateTime.Now, 
                DateTime.UtcNow.AddMinutes(5000));

            JwtSecurityToken token = new JwtSecurityToken(header, payload);
            
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
