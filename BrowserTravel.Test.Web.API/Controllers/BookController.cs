﻿using BrowserTravel.Test.API.Middlewares.Models;
using BrowserTravel.Test.API.Responses;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Interfaces.Services;
using BrowserTravel.Test.Core.QueryFilters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BrowserTravel.Test.API.Controllers
{
    /// <summary>
    /// Controlador de libros
    /// </summary>
    [Authorize]
    [Route("api/Book")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;
        private readonly IEditorialService _editorialService;

        public BookController(IBookService bookService, IEditorialService editorialService)
        {
            _bookService = bookService;
            _editorialService = editorialService;
        }

        /// <summary>
        /// Metodo para obtener todos los libros
        /// </summary>
        /// <returns>Retorna un listado de libros</returns>
        /// <response code="200">Obtener libros satisfactoriamente</response>
        /// <response code="204">No contiene libros</response>
        /// <response code="401">No Autorizado</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponse<IEnumerable<BookDto>>))]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Get([FromQuery] BaseFilters filters)
        {
            IEnumerable<BookDto> books = await _bookService.GetBooksAsync(filters);

            if (books == null || !books.Any())
                return NoContent();

            ApiResponse<IEnumerable<BookDto>> response = new ApiResponse<IEnumerable<BookDto>>(books);

            return Ok(response);
        }

        /// <summary>
        /// Metodo para obtener detalle del libro
        /// </summary>
        /// <returns>Retorna el detalle del libro</returns>
        /// <response code="200">Obtener detalle del libro satisfactoriamente</response>
        /// <response code="204">No contiene detalle</response>
        /// <response code="401">No Autorizado</response>
        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ApiResponse<BookDetailDto>))]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> GetDetail(int id)
        {
            BookDetailDto book = await _bookService.GetDetailBook(id);

            if (book == null)
                return NotFound(new ErrorDetails
                {
                    StatusCode = (int)HttpStatusCode.NotFound,
                    Message = "Url no valida"
                });

            ApiResponse<BookDetailDto> response = new ApiResponse<BookDetailDto>(book);

            return Ok(response);
        }

        /// <summary>
        /// Metodo para crear libro
        /// </summary>
        /// <returns>Retorna el libro creado</returns>
        /// <response code="201">libro creado satisfactoriamente</response>
        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ApiResponse<BookDto>))]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> CreateBook([FromBody] BookDetailDto bookDetailDto)
        {
            EditorialDto editorial = new EditorialDto
            {
                Name = bookDetailDto.Editorial,
                Campus = bookDetailDto.Campus
            };
            EditorialDto editorialDto = await _editorialService.CreateEditorialAsync(editorial);

            BookDto book = await _bookService.CreateBookAsync(bookDetailDto, editorialDto.Id);
            ApiResponse<BookDto> response = new ApiResponse<BookDto>(book);

            return CreatedAtAction(nameof(Get), new { isbn = book.ISBN }, response);
        }

        /// <summary>
        /// Metodo para eliminar libro
        /// </summary>
        /// <param name="id">ISBN del libro</param>
        /// <returns>Retorna el codigo de estado en OK</returns>
        /// <response code="200">Eliminar libro satisfactoriamente</response>
        /// <response code="401">No Autorizado</response>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> DeleteBook(int id)
        {
            await _bookService.DeleteBookAsync(id);
            return Ok();
        }

        /// <summary>
        /// Metodo para actualizar libro
        /// </summary>
        /// <param name="bookDto">Entidad dto de libro</param>
        /// <param name="id">ISBN del libro</param>
        /// <returns>Retorna el codigo de estado en OK</returns>
        /// <response code="200">Eliminar libro satisfactoriamente</response>
        /// <response code="400">Error del cliente</response>
        /// <response code="401">No Autorizado</response>
        [HttpPut]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateBook([FromBody] BookDto bookDto, int id)
        {
            if (id != bookDto.ISBN)
                return BadRequest("Los ids no coinciden");

            await _bookService.DeleteBookAsync(id);
            return Ok();
        }
    }
}