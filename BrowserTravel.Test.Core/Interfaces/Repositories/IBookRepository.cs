﻿using BrowserTravel.Test.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Repositories
{
    /// <summary>
    /// Interface de repositorio para libros
    /// </summary>
    public interface IBookRepository
    {
        /// <summary>
        /// Obtener un listado de libros de manera asíncronica
        /// </summary>
        /// <returns>Listado de libros</returns>
        Task<IEnumerable<Book>> GetBooksAsync();
        /// <summary>
        /// Obtener un listado de libros 
        /// </summary>
        /// <returns>Listado de libros</returns>
        IEnumerable<Book> GetBooks();
        /// <summary>
        /// Obtener el detalle de un libro por medio del id
        /// </summary>
        /// <param name="id">id del libro</param>
        /// <returns>retorna la consulta del detalle</returns>
        Task<Book> GetDetailBook(int id);
        /// <summary>
        /// Crear libro
        /// </summary>
        /// <param name="book">Entidad del libro</param>
        /// <returns>Retorna el libro creado</returns>
        Task<Book> CreateBookAsync(Book book, List<Author> authorList);
        /// <summary>
        /// Obtener libro por id
        /// </summary>
        /// <param name="id">ISBN del libro</param>
        /// <returns>Retorna el libro segun el id</returns>
        Task<Book> GetBookByIdAsync(int id);
        /// <summary>
        /// Eliminar libro
        /// </summary>
        /// <param name="id">ISBN del libro</param>
        Task DeleteBookAsync(int id);
        /// <summary>
        /// Actualizar libro
        /// </summary>
        /// <param name="id">ISBN del libro</param>
        /// <param name="book">Entidad libro</param>
        Task<Book> UpdateBookAsync(Book book, int id);
    }
}
