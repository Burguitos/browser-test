﻿using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Repositories
{
    /// <summary>
    /// Interface de respositorio para autor
    /// </summary>
    public interface IAuthorRepository : IGenericRepository<Author>
    {
        /// <summary>
        /// Crear autor
        /// </summary>
        /// <param name="author">Entidad del autor</param>
        /// <returns>Retorna el autor creado</returns>
        Task<Author> CreateAuthorAsync(Author author);
    }
}
