﻿using BrowserTravel.Test.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Repositories
{
    /// <summary>
    /// Interface de repositorio de editorial
    /// </summary>
    public interface IEditorialRepository : IGenericRepository<Editorial>
    {
        /// <summary>
        /// Crear editorial
        /// </summary>
        /// <param name="editorial">Entidad dto de editorial</param>
        /// <returns>Retorna la editorial creada</returns>
        Task<Editorial> CreateEditorialAsync(Editorial editorial);
        /// <summary>
        /// Obtener editorial por medio del id
        /// </summary>
        /// <param name="id">Identificador unico de editorial</param>
        /// <returns>Retorna la editorial</returns>
        Task<Editorial> GetEditorialByIdAsync(int id);
    }
}
