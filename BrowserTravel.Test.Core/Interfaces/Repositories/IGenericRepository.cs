﻿using BrowserTravel.Test.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Repositories
{
    /// <summary>
    /// Intreface generica con los metodos basicos (CRUD)
    /// </summary>
    /// <typeparam name="T">Entidad genrica</typeparam>
    public interface IGenericRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Metodo para obtener todos los datos de la entidad generica
        /// </summary>
        /// <returns>Retorna listado de la entidad</returns>
        Task<IEnumerable<T>> GetAll();
        /// <summary>
        /// Metodo para obtener entidad segun el id
        /// </summary>
        /// <param name="id">Identificador unico de la entidad</param>
        /// <returns>Retorna la entidad</returns>
        Task<T> GetById(int id);
        /// <summary>
        /// Metodo para agregar una nueva entidad
        /// </summary>
        /// <param name="id">Identificador unico de la entidad</param>
        Task Add(T entity);
        /// <summary>
        /// Metodo para actualizar la entidad
        /// </summary>
        /// <param name="id">Identificador unico de la entidad</param>
        Task Update(T entity);
        /// <summary>
        /// MEtodo para eliminar la entidad
        /// </summary>
        /// <param name="id">Identificador unico de la entidad</param>
        Task Delete(int id);
        
    }
}
