﻿using BrowserTravel.Test.Core.Interfaces.Repositories;
using System;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces
{
    /// <summary>
    /// Interface para unidad de trabajo
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Interface de libro
        /// </summary>
        IBookRepository BookRepository { get; }
        /// <summary>
        /// interface de autor
        /// </summary>
        IAuthorRepository AuthorRepository { get; }
        /// <summary>
        /// interface de editorial
        /// </summary>
        IEditorialRepository EditorialRepository { get; }
        /// <summary>
        /// Guardar cambios
        /// </summary>
        void SaveChanges();
        /// <summary>
        /// Guardar cambios asincronos
        /// </summary>
        Task SaveChangesAsync();
    }
}
