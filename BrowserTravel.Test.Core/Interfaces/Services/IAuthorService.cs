﻿using BrowserTravel.Test.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Services
{
    /// <summary>
    /// Interface de servicio para autor
    /// </summary>
    public interface IAuthorService
    {
        /// <summary>
        /// Crear autor
        /// </summary>
        /// <param name="authorDto">Entidad dto del autor</param>
        /// <returns>Retorna el autor creado</returns>
        Task<AuthorDto> CreateAuthorAsync(AuthorDto authorDto);
    }
}
