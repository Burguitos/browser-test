﻿using BrowserTravel.Test.Core.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Services
{
    /// <summary>
    /// Interface de servicio para editorial
    /// </summary>
    public interface IEditorialService
    {
        /// <summary>
        /// Crear editorial
        /// </summary>
        /// <param name="editorialDto">Entidad dto de editorial</param>
        /// <returns>Retorna la editorial creada</returns>
        Task<EditorialDto> CreateEditorialAsync(EditorialDto editorialDto);
    }
}
