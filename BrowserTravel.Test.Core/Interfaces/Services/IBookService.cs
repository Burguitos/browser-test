﻿using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.QueryFilters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Interfaces.Services
{
    /// <summary>
    /// Interface de servicio para libros
    /// </summary>
    public interface IBookService
    {
        /// <summary>
        /// Obtener un listado de libros de manera asíncronica
        /// </summary>
        /// <param name="filters">Filtros para la paginaciondel listado</param>
        /// <returns>Listado de libros</returns>
        Task<IEnumerable<BookDto>> GetBooksAsync(BaseFilters filters);
        /// <summary>
        /// Obtener un listado de libros 
        /// </summary>
        /// <returns>Listado de libros</returns>
        IEnumerable<BookDto> GetBooks();
        /// <summary>
        /// Obtener el detalle de un libro por medio del id
        /// </summary>
        /// <param name="id">id del libro</param>
        /// <returns>retorna el detalle del libro</returns>
        Task<BookDetailDto> GetDetailBook(int id);
        /// <summary>
        /// Crear libro
        /// </summary>
        /// <param name="bookDetailDto">Entidad dto del detalle del libro</param>
        /// <returns>Retorna el libro creado</returns>
        Task<BookDto> CreateBookAsync(BookDetailDto bookDetailDto, int editorialId);
        /// <summary>
        /// Eliminar libro
        /// </summary>
        /// <param name="id">ISBN del libro</param>
        Task DeleteBookAsync(int id);
    }
}
