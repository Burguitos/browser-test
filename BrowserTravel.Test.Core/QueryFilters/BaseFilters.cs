﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.QueryFilters
{
    public class BaseFilters
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
