﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Models
{
    /// <summary>
    /// Clase para la manipulacion de la paginacion
    /// </summary>
    /// <typeparam name="T"> Lista generica</typeparam>
    public class PagedList<T> : List<T>
    {
        /// <summary>
        /// Propiedad de pagina actual
        /// </summary>
        public int CurrrentPage { get; set; }
        /// <summary>
        /// Propiedad de tamaño de pagina
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Propiedad cantidad total
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// Propiedad total de paginas
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        /// Propiedad tiene pagina siguiente
        /// </summary>
        public bool HasNextPage => CurrrentPage < TotalPages;
        /// <summary>
        /// Propiedad tiene pagina anterior
        /// </summary>
        public bool HasPreviousPage => CurrrentPage > 1;
        /// <summary>
        /// Propiedad numero de pagina siguiente
        /// </summary>
        public int? NextPageNumber => HasNextPage ? CurrrentPage + 1 : null;
        /// <summary>
        /// Propiedad de numero de pagina anterior
        /// </summary>
        public int? PreviousPageNumber => HasPreviousPage ? CurrrentPage - 1 : null;

        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            TotalCount = count;
            PageSize = pageSize;
            CurrrentPage = pageNumber;
            TotalPages = (int)Math.Ceiling(Count / (double)pageSize);

            AddRange(items);
        }

        /// <summary>
        /// Metodo para crear paginacion
        /// </summary>
        /// <param name="source">Enumercion de entidad generica</param>
        /// <param name="pageNumber">Numero de pagina</param>
        /// <param name="pageSize">Tamaño de pagina</param>
        /// <returns>Retorna un listado paginado</returns>
        public static PagedList<T> Create(IEnumerable<T> source, int pageNumber, int pageSize)
        {
            List<T> items = source.ToList();
            int count = source.Count();

            if(pageNumber != 0 && pageSize != 0)
                items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedList<T>(items, count, pageNumber, pageSize);
        }
    }
}
