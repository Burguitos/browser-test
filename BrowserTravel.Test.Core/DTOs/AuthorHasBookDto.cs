﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.DTOs
{
    public class AuthorHasBookDto
    {
        public int AuthorId { get; set; }
        public int BookISBN { get; set; }
    }
}
