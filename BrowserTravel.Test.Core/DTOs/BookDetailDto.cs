﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.DTOs
{
    public class BookDetailDto
    {
        public int ISBN { get; set; }
        public string Title { get; set; }
        public string Synapse { get; set; }
        public string NumberPages { get; set; }
        public List<AuthorDto> AuthorList { get; set; }
        public string Editorial { get; set; }
        public string Campus { get; set; }
    }
}
