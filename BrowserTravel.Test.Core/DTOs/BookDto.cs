﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.DTOs
{
    public class BookDto
    {
        public int ISBN { get; set; }
        public int EditorialId { get; set; }
        public string Title { get; set; }
        public string Synapse { get; set; }
        public string NumberPages { get; set; }
    }
}
