﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Entities
{
    public class AuthorHasBook
    {
        public int AuthorId { get; set; }
        public int BookISBN { get; set; }

        public virtual Author Authors { get; set; }
        public virtual Book Books { get; set; }
    }
}
