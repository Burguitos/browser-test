﻿using System.Collections.Generic;

namespace BrowserTravel.Test.Core.Entities
{
    public class Author : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public virtual ICollection<AuthorHasBook> AuthorsHasBooks { get; set; }
    }
}
