﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Entities
{
    public class Editorial : BaseEntity
    {
        public string Name { get; set; }
        public string Campus { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
