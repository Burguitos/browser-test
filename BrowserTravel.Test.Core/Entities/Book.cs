﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Entities
{
    public class Book
    {
        public int ISBN { get; set; }
        public int EditorialId { get; set; }
        public string Title { get; set; }
        public string Synapse { get; set; }
        public string NumberPages { get; set; }

        public virtual Editorial Editorials { get; set; }
        public virtual ICollection<AuthorHasBook> AuthorsHasBooks { get; set; }
    }
}
