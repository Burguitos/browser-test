﻿using AutoMapper;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Core.Interfaces.Services;
using BrowserTravel.Test.Core.Models;
using BrowserTravel.Test.Core.QueryFilters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BookService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<BookDto> CreateBookAsync(BookDetailDto bookDetailDto, int editorialId)
        {
            List<Author> authors, authorList = new List<Author>();            

            authors = _mapper.Map<List<Author>>(bookDetailDto.AuthorList);
            authors.ForEach(x =>
            {
                Author author = _unitOfWork.AuthorRepository.CreateAuthorAsync(x).Result;
                authorList.Add(author);
            });

            Book newBook = new Book
            {
                ISBN = bookDetailDto.ISBN,
                Title = bookDetailDto.Title,
                Synapse = bookDetailDto.Synapse,
                NumberPages = bookDetailDto.NumberPages,
                EditorialId = editorialId,
                AuthorsHasBooks = new List<AuthorHasBook>()     
            };

            Book book = await _unitOfWork.BookRepository.CreateBookAsync(newBook,authorList);
            BookDto bookDto = _mapper.Map<BookDto>(book);
            return bookDto;
        }

        public async Task DeleteBookAsync(int id)
        {
            await _unitOfWork.BookRepository.DeleteBookAsync(id);
        }

        public IEnumerable<BookDto> GetBooks()
        {
            IEnumerable<Book> books = _unitOfWork.BookRepository.GetBooks();
            IEnumerable<BookDto> booksDto = _mapper.Map<IEnumerable<BookDto>>(books);
            return booksDto;
        }

        public async Task<IEnumerable<BookDto>> GetBooksAsync(BaseFilters filters)
        {
            IEnumerable<Book> books = await _unitOfWork.BookRepository.GetBooksAsync();
            IEnumerable<BookDto> booksDto = _mapper.Map<IEnumerable<BookDto>>(books);
            PagedList<BookDto> pageBooks = PagedList<BookDto>.Create(booksDto, filters.PageNumber, filters.PageSize);
            return pageBooks;
        }

        public async Task<BookDetailDto> GetDetailBook(int id)
        {
            Book existingBook = await _unitOfWork.BookRepository.GetBookByIdAsync(id);
            if (existingBook == null)
                return null;

            Book bookDetail = await _unitOfWork.BookRepository.GetDetailBook(id);
            List<Author> authors = new List<Author>();
            foreach(var item in bookDetail.AuthorsHasBooks)
            {
                authors.Add(item.Authors);
            }

            Editorial editorial = await _unitOfWork.EditorialRepository.GetEditorialByIdAsync(bookDetail.EditorialId);
            List<AuthorDto> authorsMap = _mapper.Map<List<AuthorDto>>(authors);

            BookDetailDto bookDetailDto = new BookDetailDto 
            {
                ISBN = bookDetail.ISBN,
                Title = bookDetail.Title,
                Synapse = bookDetail.Synapse,
                NumberPages = bookDetail.NumberPages,
                AuthorList = authorsMap,
                Editorial = editorial.Name,
                Campus = editorial.Campus
            };
            
            return bookDetailDto;
        }

    }
}
