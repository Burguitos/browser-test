﻿using AutoMapper;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Core.Interfaces.Services;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Services
{
    public class EditorialService : IEditorialService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public EditorialService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<EditorialDto> CreateEditorialAsync(EditorialDto editorialDto)
        {
            Editorial editorialMap = _mapper.Map<Editorial>(editorialDto);
            Editorial editorial = await _unitOfWork.EditorialRepository.CreateEditorialAsync(editorialMap);
            await _unitOfWork.SaveChangesAsync();
            EditorialDto editorialDtoMap = _mapper.Map<EditorialDto>(editorial);

            return editorialDtoMap;
        }
    }
}
