﻿using AutoMapper;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Core.Interfaces.Services;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Core.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AuthorService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<AuthorDto> CreateAuthorAsync(AuthorDto authorDto)
        {
            Author authorMap = _mapper.Map<Author>(authorDto);
            Author author = await _unitOfWork.AuthorRepository.CreateAuthorAsync(authorMap);
            await _unitOfWork.SaveChangesAsync();
            AuthorDto authorDtoMap = _mapper.Map<AuthorDto>(author);

            return authorDtoMap;
        }
    }
}
