﻿using BrowserTravel.Test.Core.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Validators
{
    public class BookDetailValidator : AbstractValidator<BookDetailDto>
    {
        public BookDetailValidator()
        {
            RuleFor(book => book.ISBN).NotNull();
            RuleFor(book => book.Title).NotNull().Length(1, 45);
            RuleFor(book => book.Synapse).NotNull().Length(1, 45);
            RuleFor(book => book.NumberPages).NotNull();
            RuleFor(book => book.Editorial).NotNull().Length(1, 45);
            RuleFor(book => book.Campus).NotNull().Length(1, 45);
            RuleFor(book => book.AuthorList).NotNull();
        }
    }
}
