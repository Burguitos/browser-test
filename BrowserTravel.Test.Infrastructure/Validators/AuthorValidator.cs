﻿using BrowserTravel.Test.Core.DTOs;
using FluentValidation;

namespace BrowserTravel.Test.Infrastructure.Validators
{
    public class AuthorValidator : AbstractValidator<AuthorDto>
    {
        public AuthorValidator()
        {
            RuleFor(author => author.Name).NotNull().Length(5,45);
            RuleFor(author => author.Surname).NotNull().Length(5,45);
        }
    }
}
