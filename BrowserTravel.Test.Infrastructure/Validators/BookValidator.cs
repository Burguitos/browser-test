﻿using BrowserTravel.Test.Core.DTOs;
using FluentValidation;

namespace BrowserTravel.Test.Infrastructure.Validators
{
    public class BookValidator : AbstractValidator<BookDto>
    {
        public BookValidator()
        {
            RuleFor(book => book.ISBN).NotNull();
            RuleFor(book => book.Title).NotNull().Length(1, 45);
            RuleFor(book => book.Synapse).NotNull().Length(1, 45);
            RuleFor(book => book.NumberPages).NotNull();
            RuleFor(book => book.EditorialId).NotNull();
        }
    }
}
