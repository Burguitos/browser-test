﻿using BrowserTravel.Test.Core.DTOs;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Validators
{
    public class EditorialValidator : AbstractValidator<EditorialDto>
    {
        public EditorialValidator()
        {
            RuleFor(editorial => editorial.Name).NotNull().Length(5, 45);
            RuleFor(editorial => editorial.Campus).NotNull().Length(5, 45);
        }
    }
}
