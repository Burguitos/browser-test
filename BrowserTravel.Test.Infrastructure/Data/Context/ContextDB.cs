﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Infrastructure.Data.Mapper;
using Microsoft.EntityFrameworkCore;

namespace BrowserTravel.Test.Infrastructure.Data.Context
{
    /// <summary>
    /// Clase del contexto de datos
    /// </summary>
    public class ContextDB : DbContext
    {
        #region DbSet

        /// <summary>
        /// Dbset para acceder a los datos de libros
        /// </summary>
        public DbSet<Book> Books { get; set; }

        /// <summary>
        /// Dbset para acceder a los datos de autores por libros 
        /// </summary>
        public DbSet<AuthorHasBook> AuthorsHasBooks { get; set; }

        /// <summary>
        /// DbSet para acceder a los datos autores
        /// </summary>
        public DbSet<Author> Authors { get; set; }

        /// <summary>
        /// Dbset para acceder a los datos editoriales
        /// <Ssummary>
        public DbSet<Editorial> Editorials { get; set; }

        #endregion

        #region Constructor

        public ContextDB(DbContextOptions<ContextDB> options) : base(options)
        {
        }

        #endregion

        #region Modelo

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookMapper());
            modelBuilder.ApplyConfiguration(new AuthorMapper());
            modelBuilder.ApplyConfiguration(new EditorialMapper());
            modelBuilder.ApplyConfiguration(new AuthorHasBookMapper());
        }
        #endregion
    }
}
