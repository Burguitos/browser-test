﻿using BrowserTravel.Test.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BrowserTravel.Test.Infrastructure.Data.Mapper
{
    /// <summary>
    /// Clase interna para mapear la configuracion de tabla y propiedades de la tabla libros
    /// </summary>
    internal class BookMapper : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("libros");

            builder.HasKey(e => e.ISBN)
                    .HasName("Pk_libros");

            builder.HasIndex(e => e.ISBN, "Ux_libros")
                    .IsUnique();

            builder.Property(e => e.ISBN).HasColumnName("ISBN").IsRequired();
            builder.Property(e => e.EditorialId).HasColumnName("editoriales_id").IsRequired();

            builder.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("titulo");

            builder.Property(e => e.Synapse)
                .IsRequired()
                .HasColumnType("ntext")
                .HasColumnName("sinopsis");

            builder.Property(e => e.NumberPages)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("n_paginas");

            builder.HasOne(d => d.Editorials)
                   .WithMany(p => p.Books)
                   .HasForeignKey(d => d.EditorialId)
                   .HasConstraintName("Fk_libros");

        }
    }
}
