﻿using BrowserTravel.Test.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Data.Mapper
{
    class EditorialMapper : IEntityTypeConfiguration<Editorial>
    {
        /// <summary>
        /// Clase interna para mapear la configuracion de tabla y propiedades de la tabla editoriales
        /// </summary>
        public void Configure(EntityTypeBuilder<Editorial> builder)
        {
            builder.ToTable("editoriales");

            builder.HasKey(e => e.Id).HasName("Pk_editoriales");
            builder.Property(e => e.Id).HasColumnName("id").IsRequired();

            builder.Property(e => e.Name)
               .IsRequired()
               .HasMaxLength(45)
               .HasColumnName("nombre");

            builder.Property(e => e.Campus)
               .IsRequired()
               .HasMaxLength(45)
               .HasColumnName("sede");
        }
    }
}
