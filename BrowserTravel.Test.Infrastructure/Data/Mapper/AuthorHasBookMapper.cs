﻿using BrowserTravel.Test.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Data.Mapper
{
    internal class AuthorHasBookMapper : IEntityTypeConfiguration<AuthorHasBook>
    {
        /// <summary>
        /// Clase interna para mapear la configuracion de tabla y propiedades de la tabla autor por libro
        /// </summary>
        public void Configure(EntityTypeBuilder<AuthorHasBook> builder)
        {
            builder.ToTable("autores_has_libros");
            builder.HasKey(e => new { e.AuthorId, e.BookISBN }).HasName("Pk_autores_has_libros"); 

            builder.Property(e => e.AuthorId).HasColumnName("autores_id").IsRequired();
            builder.Property(e => e.BookISBN).HasColumnName("libros_ISBN").IsRequired();

            builder.HasOne(d => d.Authors)
                    .WithMany(p => p.AuthorsHasBooks)
                    .HasForeignKey(d => d.AuthorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Fk2_autores_has_libros");

            builder.HasOne(d => d.Books)
                .WithMany(p => p.AuthorsHasBooks)
                .HasForeignKey(d => d.BookISBN)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("Fk1_autores_has_libros");
        }
    }
}
