﻿using BrowserTravel.Test.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BrowserTravel.Test.Infrastructure.Data.Mapper
{
    internal class AuthorMapper : IEntityTypeConfiguration<Author>
    {
        /// <summary>
        /// Clase interna para mapear la configuracion de tabla y propiedades de la tabla autores
        /// </summary>
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.ToTable("autores");

            builder.Property(e => e.Id).HasColumnName("id").IsRequired();
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Name)
               .IsRequired()
               .HasMaxLength(45)
               .HasColumnName("nombre");

            builder.Property(e => e.Surname)
               .IsRequired()
               .HasMaxLength(45)
               .HasColumnName("apellidos");
        }
    }
}
