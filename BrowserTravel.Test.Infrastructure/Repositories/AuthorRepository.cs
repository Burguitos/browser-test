﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Infrastructure.Data.Context;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Repositories
{
    public class AuthorRepository : GenericRepository<Author>, IAuthorRepository
    {
        private readonly ContextDB _db;

        public AuthorRepository(ContextDB db) : base(db)
        {
            _db = db;
        }

        public async Task<Author> CreateAuthorAsync(Author author)
        {
            Author existingAuthor = ExistingAuthor(author);
            
            if(existingAuthor != null)
                return existingAuthor;

            _entity.Add(author);
            await _db.SaveChangesAsync();

            return author;
        }

        private Author ExistingAuthor(Author author)
        {
            Author existingAuthor = _entity.Where(x => x.Name == author.Name && x.Surname == author.Surname).FirstOrDefault();
            return existingAuthor;
        }
    }
}
