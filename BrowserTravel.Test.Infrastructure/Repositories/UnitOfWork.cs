﻿using BrowserTravel.Test.Core.Interfaces;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ContextDB _db;
        private readonly IBookRepository _bookRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IEditorialRepository _editorialRepository;

        public UnitOfWork(ContextDB db)
        {
            _db = db;
        }
        public IBookRepository BookRepository => _bookRepository ?? new BookRepository(_db);

        public IAuthorRepository AuthorRepository => _authorRepository ?? new AuthorRepository(_db);

        public IEditorialRepository EditorialRepository => _editorialRepository ?? new EditorialRepository(_db);

        public void Dispose()
        {
            if (_db != null)
                _db.Dispose();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
