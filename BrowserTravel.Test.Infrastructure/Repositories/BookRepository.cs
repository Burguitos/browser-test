﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly ContextDB _db;

        public BookRepository(ContextDB db)
        {
            _db = db;
        }
        public async Task<IEnumerable<Book>> GetBooksAsync()
        {
            List<Book> books = await _db.Books.ToListAsync();
            return books;
        }

        public IEnumerable<Book> GetBooks()
        {
            List<Book> books = _db.Books.ToList();
            return books;
        }

        public async Task<Book> GetDetailBook(int id)
        {
            IIncludableQueryable<Book, Author> books = _db.Books.Where(x => x.ISBN == id)
                                    .Include(x => x.AuthorsHasBooks)
                                    .ThenInclude(x => x.Authors);

            Book book = await books.FirstAsync();

            return book;
        }

        public async Task<Book> CreateBookAsync(Book book, List<Author> authorList)
        {
            Book existingBook = ExistingBook(book);
            
            if (existingBook != null)            
                return existingBook;

            foreach (Author author in authorList)
            {
                book.AuthorsHasBooks.Add(new AuthorHasBook
                {
                    Authors = author,
                    Books = book
                });
            }

            _db.Books.Add(book);
            await _db.SaveChangesAsync();

            return book;
        }

        public async Task<Book> GetBookByIdAsync(int id)
        {
            Book book = await _db.Books.FindAsync(id);
            return book;
        }

        public async Task DeleteBookAsync(int id)
        {
            Book existingBook = _db.Books.Include(x => x.AuthorsHasBooks).Single(x => x.ISBN == id);
            if(existingBook != null)
            {
                List<AuthorHasBook> book = existingBook.AuthorsHasBooks.ToList();
                book.ForEach(x => { _db.Remove(x); });             
                _db.Remove(existingBook);
                await _db.SaveChangesAsync();
            }
        }

        private Book ExistingBook(Book book)
        {
            Book existingBook = _db.Books
                                .Include(p => p.AuthorsHasBooks)
                                .Where(p => p.ISBN == book.ISBN).FirstOrDefault();

            return existingBook;
        }

        public async Task<Book> UpdateBookAsync(Book book, int id)
        {
            Book bookDetail = await GetDetailBook(id);
            return bookDetail;
            //foreach (AuthorHasBook item in bookDetail.AuthorsHasBooks)
            //{
            //    if(item.Authors.)
            //}

            //var book = 
        }
    }
}
