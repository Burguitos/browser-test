﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Repositories
{
    public class EditorialRepository : GenericRepository<Editorial>, IEditorialRepository
    {
        private readonly ContextDB _db;

        public EditorialRepository(ContextDB db) : base(db)
        {
            _db = db;
        }
        public async Task<Editorial> CreateEditorialAsync(Editorial editorial)
        {
            Editorial existingEditorial = ExistingEditorial(editorial);

            if (existingEditorial != null)
                return existingEditorial;

            _entity.Add(editorial);
            await _db.SaveChangesAsync();

            return editorial;  
        }

        public async Task<Editorial> GetEditorialByIdAsync(int id)
        {
            Editorial editorial = await _entity.FindAsync(id);
            return editorial;
        }

        private Editorial ExistingEditorial(Editorial editorial)
        {
            Editorial existingEditorial = _entity.Where(x => x.Name == editorial.Name && x.Campus == editorial.Campus).FirstOrDefault();
            return existingEditorial;
        }
    }
}
