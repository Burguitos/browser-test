﻿using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.Infrastructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly ContextDB _db;
        protected readonly DbSet<T> _entity;

        public GenericRepository(ContextDB db)
        {
            _db = db;
            _entity = db.Set<T>();
        }
        public async Task Add(T entity)
        {   
            _entity.Add(entity);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            T entity = await GetById(id);
            _entity.Remove(entity);
            _db.SaveChanges();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            IEnumerable<T> entities = await _entity.ToListAsync();
            return entities;
        }

        public async Task<T> GetById(int id)
        {
            T entity = await _entity.FindAsync(id);
            return entity;
        }

        public async Task Update(T entity)
        {
            _entity.Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
