﻿using Microsoft.Extensions.Configuration;
using System;

namespace Browser.Test.UnitTest
{
    /// <summary>
    /// Clase base para las pruebas
    /// </summary>
    public class UnitApiTestBase
    {
        public static IConfiguration GetConfiguration()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
    }
}
