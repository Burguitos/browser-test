﻿using BrowserTravel.Test.API.Controllers;
using BrowserTravel.Test.Core.Entities;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace Browser.Test.UnitTest.UnitTest.API.TokenTest.Controller
{
    /// <summary>
    /// Clase de pruebas unitarias para el controlador de libros
    /// </summary>
    public class AuthenticationTokenTest : UnitApiTestBase
    {
        #region Metodos pruebas

        /// <summary>
        /// Metodo de prueba para validar respuesta status code Ok
        /// </summary>
        [Test]
        public async Task Validate_Response_Controller_Ok()
        {
            TokenController token = new TokenController(GetConfiguration());

            IActionResult response = await token.Authentication(new UserLogin { User = "", Password = "" });

            var result = (response as OkObjectResult);

            Assert.IsNotNull(response);
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        #endregion
    }
}
