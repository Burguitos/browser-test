﻿using AutoMapper;
using BrowserTravel.Test.API.Controllers;
using BrowserTravel.Test.API.Profiles;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Interfaces.Services;
using BrowserTravel.Test.Core.QueryFilters;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Browser.Test.UnitTest.UnitTest.API.BookTest.Controller
{
    /// <summary>
    /// Clase de pruebas unitarias para el controlador de libros
    /// </summary>
    [TestFixture]
    public class GetAllBookTest
    {
        #region Propiedades

        private IEnumerable<BookDto> _books;

        #endregion

        #region Metodos pruebas

        /// <summary>
        /// Metodo de prueba para validar respuesta status code Not Content
        /// </summary>
        [Test]
        public async Task Validate_Response_Controller_No_Content()
        {
            Mock<IBookService> _mockBookService = new Mock<IBookService>();
            _mockBookService.Setup(m => m.GetBooksAsync(It.IsAny<BaseFilters>())).ReturnsAsync(It.IsAny<IEnumerable<BookDto>>());
            
            BookController book = new BookController(_mockBookService.Object, It.IsAny<IEditorialService>());
            IActionResult response = await book.Get(new BaseFilters());

            var result = (response as NoContentResult);

            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.NoContent, result.StatusCode);
        }

        /// <summary>
        /// Metodo de prueba para validar respuesta status code Ok
        /// </summary>
        [Test]
        public async Task Validate_Response_Controller_OK()
        {
            _books = Enumerable.Range(1, 4).Select(x => new BookDto()
            {
                ISBN = x,
                Title = $"Titulo {x}",
                Synapse = $"Sinapsis {x}",
                NumberPages = $"{x}",
                EditorialId = x
            });

            Mock<IBookService> _mockBookService = new Mock<IBookService>();
            IEnumerable<BookDto> books = _books;
            _mockBookService.Setup(m => m.GetBooksAsync(It.IsAny<BaseFilters>())).ReturnsAsync(books);

            BookController book = new BookController(_mockBookService.Object, It.IsAny<IEditorialService>());
            IActionResult response = await book.Get(new BaseFilters());

            OkObjectResult result = (response as OkObjectResult);

            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.OK, result.StatusCode);
        }

        #endregion
    }
}
