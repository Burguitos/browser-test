﻿using AutoMapper;
using BrowserTravel.Test.API.Profiles;
using BrowserTravel.Test.Core.DTOs;
using BrowserTravel.Test.Core.Entities;
using BrowserTravel.Test.Core.Interfaces;
using BrowserTravel.Test.Core.Interfaces.Repositories;
using BrowserTravel.Test.Core.QueryFilters;
using BrowserTravel.Test.Core.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Browser.Test.UnitTest.UnitTest.API.BookTest.Core
{
    /// <summary>
    /// Clase de pruebas unitarias para los servicios de libros
    /// </summary>
    public class GetBooksCoreTest
    {
        #region Variables Globales

        private IEnumerable<Book> _books;
        private IMapper _mapper;

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo privado para crear lista de libros de prueba
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Book> GetBooks()
        {
            _books = Enumerable.Range(1, 4).Select(x => new Book()
            {
                ISBN = x,
                Title = $"Titulo {x}",
                Synapse = $"Sinapsis {x}",
                NumberPages = $"{x}",
                EditorialId = x

            });
            return _books;
        }

        #endregion

        #region Inicio

        [SetUp]
        public void SetUp()
        {
            MapperConfiguration config = DependecyInjectionMapper.GetMapperConfig();
            _mapper = config.CreateMapper();

            GetBooks();
        }

        #endregion

        #region Pruebas

        /// <summary>
        /// Metodo de prueba para verificar todas las propiedades de la lista de libros
        /// </summary>
        [Test]
        public async Task Validate_Book_List_All_Propierties()
        {
            Mock<IBookRepository> _mockRepository = new Mock<IBookRepository>();
            _mockRepository.Setup(r => r.GetBooksAsync()).ReturnsAsync(_books);
            IEnumerable<BookDto> booksDto = _mapper.Map<IEnumerable<BookDto>>(_books);

            Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockUnitOfWork.Setup(r => r.BookRepository).Returns(_mockRepository.Object);

            BookService bookService = new BookService(_mockUnitOfWork.Object, _mapper);
            IEnumerable<BookDto> result = await bookService.GetBooksAsync(new BaseFilters { });            

            Assert.IsNotEmpty(result);
            Assert.IsNotNull(result);
            CollectionAssert.AllItemsAreNotNull(result);
            CollectionAssert.AllItemsAreUnique(result);
            CollectionAssert.AllItemsAreInstancesOfType(result, typeof(BookDto));
        }

        /// <summary>
        /// Metodo de prueba para verficar si la lista de libros es vacia
        /// </summary>
        [Test]
        public async Task Validate_Device_List_Is_Empty()
        {
            Mock<IBookRepository> _mockRepository = new Mock<IBookRepository>();
            _books = new List<Book>();
            _mockRepository.Setup(r => r.GetBooksAsync()).ReturnsAsync(_books);

            Mock<IUnitOfWork> _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockUnitOfWork.Setup(r => r.BookRepository).Returns(_mockRepository.Object);

            BookService bookService = new BookService(_mockUnitOfWork.Object, _mapper);
            IEnumerable<BookDto> result = await bookService.GetBooksAsync(new BaseFilters { });

            Assert.IsEmpty(result);            
        }

        #endregion
    }
}
