﻿using BrowserTravel.Test.WebAplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BrowserTravel.Test.WebAplication.Controllers
{
    public class BookController : Controller
    {
        private static BookDetailViewModel _currentBook;
        private static BookDetailViewModel CurrentBook
        {
            get
            {
                if (_currentBook == null)
                    _currentBook = GetFakeBook();
                return _currentBook;
            }
            set
            {
                _currentBook = value;
            }
        }

        private readonly Uri baseUrl = new Uri("https://localhost:44326/api/");
        private readonly HttpClient client;
        private readonly ILogger<BookController> _logger;

        public BookController(ILogger<BookController> logger)
        {
            _logger = logger;
            client = new HttpClient();
            client.BaseAddress = baseUrl;
        }

        private static BookDetailViewModel GetFakeBook()
        {
            return new BookDetailViewModel()
            {
                ISBN = 1,
                Title = "",
                Synapse = "",
                NumberPages = "",
                Editorial = "",
                Campus = "",
                AuthorList = new List<AuthorViewModel>()
                {
                    new AuthorViewModel() { Name="", Surname = ""}
                }
            };
        }

        // GET: BookController
        public ActionResult Index()
        {
            ApiResponseViewModel<List<BookViewModel>> books;
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("token"));
            HttpResponseMessage response = client.GetAsync("Book").Result;
            ViewBag.Token = HttpContext.Session.GetString("token");


            if (response.StatusCode == HttpStatusCode.OK)
            {
                string stringData = response.Content.ReadAsStringAsync().Result;
                books = JsonConvert.DeserializeObject<ApiResponseViewModel<List<BookViewModel>>>(stringData);
                return View(books.Data);
            }

            if (response.StatusCode == HttpStatusCode.NoContent)
                ViewBag.Message = "No hay libros en la libreria";

            if (response.StatusCode == HttpStatusCode.Unauthorized)            
                ViewBag.Message = "Debe obtener token de autorización";                    

            return View();
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("token"));
            HttpResponseMessage response = client.GetAsync($"Book/{id}").Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string stringData = response.Content.ReadAsStringAsync().Result;
                ApiResponseViewModel<BookDetailViewModel> book = JsonConvert.DeserializeObject<ApiResponseViewModel<BookDetailViewModel>>(stringData);
                return View(book.Data);
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
                ViewBag.Message = "Url no valida";

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                ViewBag.Message = "Debe obtener token de autorización";

            return View();
        }

        // GET: BookController/Create
        public ActionResult Create()
        {
            return View(CurrentBook);
        }

        public ActionResult AuthorEntryRow()
        {
            return PartialView("AuthorEntryRow");
        }

        // POST: BookController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookDetailViewModel book)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Message = "Todos lo campos deben ser diligenciados";
                return View(CurrentBook);
            }

            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);

            string stringData = JsonConvert.SerializeObject(book);
            StringContent contentData = new StringContent(stringData, Encoding.UTF8, "application/json");

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("token"));
            HttpResponseMessage response = client.PostAsync("Book", contentData).Result;

            if (response.StatusCode == HttpStatusCode.Created)
            {               
                return RedirectToAction(nameof(Index));
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                ViewBag.Message = "Para crear debe obtener token de autorización";

            return View();
        }

        // GET: BookController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BookController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: BookController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BookController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", HttpContext.Session.GetString("token"));
            HttpResponseMessage response = client.DeleteAsync($"Book?id={id}").Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return RedirectToAction(nameof(Index));
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
                ViewBag.Message = "Url no valida";

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                ViewBag.Message = "Debe obtener token de autorización";

            return View();
        }
    }
}
