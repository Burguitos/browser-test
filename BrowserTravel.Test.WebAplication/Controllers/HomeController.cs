﻿using BrowserTravel.Test.WebAplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace BrowserTravel.Test.WebAplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly Uri baseUrl = new Uri("https://localhost:44326/api/");
        private readonly HttpClient client;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;

            client = new HttpClient();
            client.BaseAddress = baseUrl;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);

            UserViewModel userModel = new UserViewModel();
            userModel.User = "User1";
            userModel.Password = "pass$word";

            string stringData = JsonConvert.SerializeObject(userModel);
            var contentData = new StringContent(stringData, Encoding.UTF8, "application/json");

            HttpResponseMessage response = client.PostAsync("Token", contentData).Result;

            if (response.IsSuccessStatusCode)
            {
                string stringJWT = response.Content.ReadAsStringAsync().Result;
                JWTViewModel jwt = JsonConvert.DeserializeObject<JWTViewModel>(stringJWT);

                HttpContext.Session.SetString("token", jwt.Token);

                ViewBag.Message = "Token obtenido satisfactoriamente";
            }

            return View("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
