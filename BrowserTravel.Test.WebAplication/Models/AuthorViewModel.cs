﻿using System.ComponentModel.DataAnnotations;

namespace BrowserTravel.Test.WebAplication.Models
{
    public class AuthorViewModel
    {
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Name { get; set; }
        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Los apellidos son requeridos")]
        public string Surname { get; set; }
    }
}
