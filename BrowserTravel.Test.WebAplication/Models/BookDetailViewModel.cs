﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BrowserTravel.Test.WebAplication.Models
{
    public class BookDetailViewModel
    {
        public int ISBN { get; set; }
        [Display(Name = "Titulo")]
        [Required(ErrorMessage = "El titulo es requerido")]

        public string Title { get; set; }
        [Display(Name = "Sinapsis")]
        [Required(ErrorMessage = "la sinapsis es requerida")]
        public string Synapse { get; set; }
        [Display(Name = "N° Paginas")]
        [Required(ErrorMessage = "El N° paginas es requerido")]
        public string NumberPages { get; set; }
        [Required(ErrorMessage = "La editorial es requerida")]
        public string Editorial { get; set; }
        [Display(Name = "Sede")]
        [Required(ErrorMessage = "La sede es requerida")]
        public string Campus { get; set; }
        public IList<AuthorViewModel> AuthorList { get; set; }
    }
}
