﻿namespace BrowserTravel.Test.WebAplication.Models
{
    public class JWTViewModel
    {
        public string Token { get; set; }
    }
}
