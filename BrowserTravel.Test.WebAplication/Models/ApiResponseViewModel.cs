﻿namespace BrowserTravel.Test.WebAplication.Models
{
    public class ApiResponseViewModel<T> where T : class
    {
        public T Data { get; set; }
    }
}
