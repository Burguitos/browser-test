﻿namespace BrowserTravel.Test.WebAplication.Models
{
    public class BookViewModel
    {
        public int ISBN { get; set; }
        public int EditorialId { get; set; }
        public string Title { get; set; }
        public string Synapse { get; set; }
        public string NumberPages { get; set; }
    }
}
