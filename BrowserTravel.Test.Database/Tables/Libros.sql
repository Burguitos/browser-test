﻿CREATE TABLE [dbo].[libros]
(
	[ISBN]              INT             NOT NULL, 
    [editoriales_id]    INT             NOT NULL, 
    [titulo]            NVARCHAR(45)    NOT NULL, 
    [sinopsis]          NTEXT           NOT NULL, 
    [n_paginas]         NVARCHAR(45)    NOT NULL
)
GO

ALTER TABLE [dbo].[libros]
    ADD CONSTRAINT Pk_libros PRIMARY KEY ([ISBN])
GO

ALTER TABLE [dbo].[libros]
    ADD CONSTRAINT Ux_libros UNIQUE ([ISBN])
GO

ALTER TABLE [dbo].[libros]
    ADD CONSTRAINT Fk_libros FOREIGN KEY ([editoriales_id]) REFERENCES [dbo].[editoriales] ([id]) ON DELETE CASCADE
GO

