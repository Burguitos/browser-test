﻿CREATE TABLE [dbo].[autores]
(
	[id]        INT             IDENTITY NOT NULL, 
    [nombre]    NVARCHAR(45)    NOT NULL, 
    [apellidos] NVARCHAR(45)    NOT NULL
)
GO

ALTER TABLE [dbo].[autores]
    ADD CONSTRAINT [Pk_autores] PRIMARY KEY([id])
GO
