﻿CREATE TABLE [dbo].[autores_has_libros]
(
	[autores_id]	INT		NOT NULL, 
    [libros_ISBN]	INT		NOT NULL
)
GO

ALTER TABLE [dbo].[autores_has_libros]
	ADD CONSTRAINT [Fk1_autores_has_libros]  FOREIGN KEY([libros_ISBN]) REFERENCES [dbo].[libros]([ISBN])
GO

ALTER TABLE [dbo].[autores_has_libros]
	ADD CONSTRAINT [Fk2_autores_has_libros]  FOREIGN KEY([autores_id]) REFERENCES [dbo].[autores]([id])
GO


