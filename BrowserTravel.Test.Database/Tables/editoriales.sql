﻿CREATE TABLE [dbo].[editoriales]
(
	[id]        INT             IDENTITY NOT NULL, 
    [nombre]    NVARCHAR(45)    NOT NULL, 
    [sede]      NVARCHAR(45)    NOT NULL
)
GO

ALTER TABLE [dbo].[editoriales]
    ADD CONSTRAINT Pk_editoriales PRIMARY KEY ([id])
GO